#encoding "utf-8"  
#GRAMMAR_ROOT Root  

NL -> Word<kwtype="newline">;
WordExp -> Word<kwtype=~"newline"> | "&" | "/";
Break -> NL | Punct | "(" | ")" | "-" | "," | "<" | ">" | EOSent;

Abbr -> Word<kwtype="position_abbrev">; 
Level -> Word<kwtype="positions-levels">; 

SimplePosition -> Word<kwtype="positions">;

SimplePositionWithLevel -> Level SimplePosition;
SimplePositionWithLeftDescr -> WordExp+ SimplePosition;

PositionBase -> SimplePosition | SimplePositionWithLeftDescr | SimplePositionWithLevel | Abbr;

Preps -> "of" | "to" | "in" | "for" | "at";
PositionWithPrep -> PositionBase Preps (NL<cut>) WordExp+;
PositionWithRightDescr -> PositionBase WordExp+;
PositionWithNLDescr -> Position "and" NL<cut> WordExp+;

Position -> PositionBase | PositionWithPrep | PositionWithRightDescr | PositionWithNLDescr;

AndSign -> "&" | "and" | "-" | "/" | ",";
MultiplePositionsLink -> NL | AndSign | NL<cut> AndSign | AndSign NL<cut>;
MultiplePositions -> Position (MultiplePositionsLink) Position;
MultiplePositions -> MultiplePositions MultiplePositionsLink Position;

PositionClause -> Position | MultiplePositions;
Position_interp -> AnyWord NL PositionClause interp (Position.Name) Break;
Position_interp -> Break PositionClause interp (Position.Name) Break;
Position_interp -> PositionClause<fw> interp (Position.Name) Break;

Root -> Position_interp;