import os
import sys
import re 
import json

import talon
from talon.signature.bruteforce import extract_signature
from talon import quotations

talon.init()
from talon import signature

def cut_meta_info(message_by_line):
    message_start_index = 0

    # cut meta information 
    for i in message_by_line:
        if i.startswith('X-FileName: '):
            message_start_index = message_by_line.index(i) + 1 
            break

    # cut attachments        
    message = list(filter(lambda item: 
        re.search('\.(doc|dat|docx|pdf|xls|xlsx|txt|jpg|mem|html|wav|mp3|mp4|ppt|wpd|vcf|dtf|htm|url|pcx)', 
            item.lower()) == None, message_by_line[message_start_index:]))

    return message    


def extract_signature_with_talon(message_by_line):
    sender = message_by_line[2][6:]
    message = ''.join(message_by_line)
    
    text, signature = extract_signature(message)
    if (signature is None):
        text, signature = talon.signature.extract(message, sender=sender)    
    if (signature is None):
        signature = "" 
    return signature
        

def extract_position_with_tomita(signature):
    with open('tomita-files/signature.txt', 'w') as signature_file:
        # convert signature to one line
        signature_file.write(signature.replace('\n\n', '\n').replace('\n', ' NEWLINE '))
        signature_file.close()

    os.system('./tomita-parser/build/bin/tomita-parser config.proto')
    try:
        with open('tomita-files/facts.json') as f:
            text = f.read()
            if (text):
                # choose first json from tomita output
                if '][' in text:
                    text = text[:text.find('][')+1]
                
                facts = json.loads(text)
                if len(facts) > 0:
                    position = facts[0]["FactGroup"][0]["Fact"][0]["Field"][0]["Value"].lower()
                    position = position.replace(" newline", ",")
                    return position
                else:
                    return 'unknown'
            else:
                return 'unknown'            
    except:
        return 'unknown'            


def main(message_by_line):
    message_by_line = cut_meta_info(message_by_line)
    signature = extract_signature_with_talon(message_by_line)
    position = extract_position_with_tomita(signature)

    return position
    

if __name__ == "__main__":
    if (sys.argv[1]):
        try:
            with open(sys.argv[1]) as f:
                text = f.readlines()
                position = main(text)
                print (position)
        except:
            print ("Failed to open the file " + sys.argv[1])
    else:
        print ("Filename should be provided as an argument")        
