import sys

import extract_position

def main(text):
    position = extract_position.extract_position_with_tomita(''.join(text))

    return position
    
       
if __name__ == "__main__":
    if (sys.argv[1]):
        try:
            with open(sys.argv[1]) as f:
                text = f.readlines()
                position = main(text)
                print (position)
        except:
            print ("Failed to open the file " + sys.argv[1])
    else:
        print ("Filename should be provided as an argument")     