# Position extractor #

## Files: ##

* 'tomita-parser' directory  
tomita-parser build  
how to install: https://github.com/yandex/tomita-parser 

* 'tomita-files' directory  
 files, that need to build tomita-project. contains gazeteers and rule-based grammatics.

* 'dev-corpus' directory  
 dev_mails - 200 files with mails  
 dev_signatires - 317 files with signatures (extracted with talon)
 dev_mails_canon - table with filename with mail and manually extracted position from it  
 dev_signatures_canon - table with filename with signature and manually extracted position from it

* 'test-corpus' directory  
 test_mails - 220 files with mails  
 test_signatires - 220 files with signatures (extracted with talon)  
 test_mails_canon - table with filename with mail and manually extracted position from it  
 test_signatures_canon - table with filename with signature and manually extracted position from it

* 'results' directory  
 accuracy of position-extractor work on different corpora

* extract_position.py  
 python module, gets filename with a mail-body as an argument, print position of the mail's author as an output

* extract_position_from_signature.py   
 python module, gets filename with a signature-text as an argument, print position of the mail's author as an output

* eval_mails.py  
 count accuracy of position-extractor on mails dev or test corpus, gets 'dev' or 'test' as an argument

* eval_signatures.py  
 count accuracy of position-extractor on signatures dev or test corpus, gets 'dev' or 'test' as an argument


## Data: ##

* Enron mails   
https://www.kaggle.com/wcukierski/enron-email-dataset

## Algorithm: ##

Execute with extract_position.py script.

1. Input: plain text with a mail 
2. Preprocessing: cut meta information
3. Signature extraction: extract signature using talon module. Firstly, cut the quotations, than use combination of rule-based and machine-lerning talon algorithms. Then siognature is converted to one line, to provide extraction of positions, that are written on separate lines.
4. Position extraction: extract position using tomita-parser. Tomita-parser uses grammatics and gazeeters to extract position.
5. Output: position 



## Result: ##

Two types of corpora are prepared for evaluating the result. 
Mails corpus consists of files with mails, it checks a composition of talon and tomita work.
Signatures corpus consists of files with signatures, that are already extracted by talon. It checks only the work of position extraction by tomita-parser.

Dev corpus was semi-automatically prepared. Talon module extracts signatures from all mails dataset, than mails with signatures, that contains positions, were manually chosen. This list of mails appeared in mails corpus, and its extracted signatures - in the signatures corpus. Than two canon files with right answers were manually prepared.   

Test corpus was also semi-automatically prepared after all the work was done. Talon module extracts signatures from all mails datasets, regarding that signature has more than 4 lines. It was needed to increase the possibility to meet position in the signature. This list of mails appeared in mails corpus, and its extracted signatures - in the signatures corpus. Than two canon files with right answers were manually prepared. 


The result is provided in accuracy for both dev and test corpus.
Accuracy is a fraction of right answers to all answers.

Accuracy:

* dev-mails-result: 51.0
* dev-signatures-result: 97.9
* test-mails-result: 86.8
* test-signatures-result: 95.9


Note:   
dev-mails-result looks so poor, because all mails in it contains a signature with position. So, regarding the good dev-signatures-result, it means that sometimes talon chooses the wrong signature. 
test-mails-corpus has a lot of mails with no position in signature, so there was nothing to extract, and if talon chose a bad signature, it does not affect the result. It explains the higher accuracy.  