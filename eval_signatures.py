import os 
import sys
import extract_position_from_signature

right_answers = 0
wrong_answers = 0

if sys.argv[1] == 'test':
    corpus = 'test'
else:
    corpus = 'dev'

with open('{0}-corpus/{0}_signatures_canon'.format(corpus)) as f:
    for line in f:
        line = line.strip().split('\t')
        mail_file = '{0}-corpus/{0}_signatures/'.format(corpus) + line[0]
        position = line[1]
        with open(mail_file) as file:
            print (line[0])
            mail = file.readlines()
            answer = extract_position_from_signature.main(mail)
            if (answer == position):
                right_answers += 1
            else:
                wrong_answers += 1
                print ('FAIL!\nanswer: ' + answer + '\ncanon: ' + position)
            accuracy = right_answers / (right_answers + wrong_answers) * 100   
    print ('Accuracy: ' + str(accuracy) + '%')    
